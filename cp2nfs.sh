#!/bin/bash

#default source path is for DEBUG.

SRCPATH="bin/Debug"
SRCFILE="as_irda"
DSTPATH="$HOME/nfs_share"

for p in $*
do
    case "$p" in
        --release)
            SRCPATH="bin/Release";;
        --clean)
            echo "Clean destination ..."
            rm -rf $DSTPATH/$SRCFILE
            exit 0;;
    esac
done

#Checking source path and file.

if [ ! -e $SRCPATH ]; then
    echo "Source path not found: $SRCPATH"
    exit 0
fi

SRCCOMPATH="$SRCPATH/$SRCFILE"

if [ ! -e $SRCCOMPATH ]; then
    echo "Compiled binary seems not be existed."
    exit 0
fi

if [ ! -e $DSTPATH ]; then
    echo "Destination path not found: $DSTPATH"
    exit 0
fi

#Copying file...

echo "Copying file $SRCFILE to $DSTPATH ..."

cp -rf $SRCCOMPATH $DSTPATH

echo "done."
