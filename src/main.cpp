#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/signal.h>
#include <sys/ioctl.h>
#include <sys/poll.h>
#include <termios.h>

#include <netinet/in.h>
#include <arpa/inet.h>

#include <pthread.h>

////////////////////////////////////////////////////////////////////////////////

#define DEF_APP_NAME        "as_irda"
#define DEF_APP_VERSION     "0.1.6.38"

#define DEF_DEV_FNAME_IRDA  "/dev/ttyS0"
#define DEF_SZ_READ         1024
#define DEF_SZ_WRITE        DEF_SZ_READ
#define DEF_PORT_TCP        8805

// 2016-07-04 : new MCU not limited to packet size.
//#define DEF_PCKT_SIZE       19

////////////////////////////////////////////////////////////////////////////////

static bool     flag_quit  = false;
static int      state_loop = 0;
static int      fdtty = -1;

static char     sock_readbuff[DEF_SZ_READ];
static char     readbuff[DEF_SZ_READ];
static char     writebuff[DEF_SZ_WRITE];
static int      writebuff_sz = 0;
static bool     writebuff_rd = false;

struct termios  tio = {0};
struct termios  prev_tio = {0};

static int      server_fdsock = -1;
static int      client_fdsock = -1;
static int      fdsock;
static int      fd_num;

static int      clients[FD_SETSIZE] = {-1};
static int      clients_max;
static socklen_t client_len;

static fd_set   readfds;
static fd_set   allfds;

struct sockaddr_in clientaddr;
struct sockaddr_in serveraddr;

pthread_t       th_socket;
pthread_t       th_serial;

bool            opt_version = false;
bool            opt_noprint = false;
bool            opt_run_d   = false;
bool            opt_help    = false;
bool            opt_forcer  = false;

////////////////////////////////////////////////////////////////////////////////

bool open_port()
{
    fdtty = open( DEF_DEV_FNAME_IRDA, O_RDWR | O_NOCTTY );

    if ( fdtty >= 0 )
    {
        tcgetattr( fdtty, &prev_tio );

        tio.c_iflag = IGNPAR;
        tio.c_oflag = 0;
        tio.c_cflag = B115200 | CS8 | CLOCAL | CREAD;
        tio.c_lflag = 0;

        tio.c_cc[VTIME] = 0;
        tio.c_cc[VMIN]  = 1;

        tcflush( fdtty, TCIFLUSH );
        tcsetattr( fdtty, TCSANOW, &tio );

        return true;
    }

    return false;
}

bool close_port()
{
#ifdef DEBUG
    printf("[close tty]");
#endif // DEBUG
    if ( fdtty >= 0 )
    {
        tcsetattr( fdtty, TCSANOW, &prev_tio );

        close( fdtty );
        fdtty = -1;

#ifdef DEBUG
    printf("[done]");
#endif // DEBUG

        return true;
    }

#ifdef DEBUG
    printf("[failure]");
#endif // DEBUG

    return false;
}

bool open_socket()
{
    if ( (server_fdsock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP )) < 0 )
    {
        perror("socket error : ");
        return false;
    }

    int optval = 1;
    setsockopt( server_fdsock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval) );

    bzero(&serveraddr, sizeof(serveraddr));

    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl( INADDR_ANY );
    serveraddr.sin_port = htons( DEF_PORT_TCP );

    int state = bind (server_fdsock, (struct sockaddr *)&serveraddr, sizeof(serveraddr));

    if (state == -1)
    {
        close( server_fdsock );
        perror("bind error : ");
        return false;
    }

    state = listen(server_fdsock, 5);
    if (state == -1)
    {
        close( server_fdsock );
        perror("listen error : ");
        return false;
    }

    return true;
}

bool close_socket()
{
#ifdef DEBUG
    printf("[close socket]");
#endif // DEBUG

    for ( int cnt=0; cnt<FD_SETSIZE; cnt++ )
    {
        if ( clients[cnt] >= 0 )
        {
#ifdef DEBUG
            printf("[%d->%d]", cnt, clients[cnt] );
#endif // DEBUG
            close( clients[cnt] );
            clients[cnt] = -1;
        }
    }

    return true;
}

static unsigned char Calc_Check_Sum(unsigned char *pData, unsigned short Num)
{
	unsigned short InitCrc = 0x100;
	unsigned short i;

	for (i=0; i<Num; i++)
	{
		InitCrc -= *pData++;
	}

	return (InitCrc & 0xff);
}

static void mk_record_on_off_command( unsigned char* TxBuf, unsigned char on_off)
{
    if ( TxBuf == NULL )
        return;

	TxBuf[0] = 0xA7;	//packet Header
	TxBuf[1] = 0x01;	//Command Packet
	TxBuf[2] = 0x02;	//Length, LSB
	TxBuf[3] = 0x00;	//Length, MSB
	TxBuf[4] = 0x02;	//Send Record On/OFF Command
	TxBuf[5] = on_off;	// 0:OFF, 1 :ON
	TxBuf[6] = Calc_Check_Sum(&TxBuf[1], 5);	//CheckSum
}

////////////////////////////////////////////////////////////////////////////////

void* do_loop_serial( void* p )
{
    state_loop = 0;

    if ( fdtty >= 0 )
    {
        state_loop = 1;

        memset( readbuff, 0, DEF_SZ_READ );

        while( flag_quit == false )
        {
            int szread = read( fdtty, readbuff, DEF_SZ_READ );
            if ( szread > 0 )
            {
#ifdef DEBUG
                printf("## TTY data came(%d bytes): ", szread);
                for( int cnt=0; cnt<szread; cnt++ )
                {
                    printf("%02X",readbuff[cnt]);
                    if( cnt+1 < szread )
                    {
                        printf(":");
                    }
                }
                printf("\n");
#endif /// of DEBUG

                if ( writebuff_rd == false )
                {
                    writebuff_rd = true;

                    memcpy( writebuff, readbuff, szread );
                    writebuff_sz = szread;

                    for (int cnt = 0; cnt<FD_SETSIZE; cnt++)
                    {
                        //write( clients[cnt], &writebuff[ foundStart ], DEF_PCKT_SIZE );
                        send( clients[cnt], writebuff, szread, MSG_NOSIGNAL );
                    }

                    writebuff_rd = false;
                }
            }
        }
    }

    state_loop = -1;

    return NULL;
}

void* do_loop_socket( void* p )
{
    client_fdsock = server_fdsock;

    int maxi = -1;

    int maxfd = server_fdsock;

    for (int cnt = 0; cnt<FD_SETSIZE; cnt++)
    {
        clients[cnt] = -1;
    }

    FD_ZERO(&readfds);
    FD_SET(server_fdsock, &readfds);

    while( flag_quit == false )
    {
        if ( writebuff_rd == true )
            continue;

        int cnt = 0;
        allfds = readfds;
        fd_num = select(maxfd + 1 , &allfds, (fd_set *)0, (fd_set *)0, NULL);

        if (FD_ISSET(server_fdsock, &allfds))
        {
            client_len = sizeof(clientaddr);
            // accept 한다.
            client_fdsock = accept(server_fdsock, (struct sockaddr *)&clientaddr, &client_len);

            for (cnt=0; cnt<FD_SETSIZE; cnt++)
            {
                if ( clients[cnt] < 0 )
                {
                    clients[cnt] = client_fdsock;
#ifdef DEBUG
                    printf("# currnet socket = %d : %d\n", cnt, client_fdsock);
#endif /// of DEBUG
                    break;
                }
            }

            if ( cnt == FD_SETSIZE )
            {
                close( fdsock );
                clients[cnt] = -1;
            }

            FD_SET(client_fdsock,&readfds);

            if ( client_fdsock > maxfd )
                maxfd = client_fdsock;

            if ( cnt > maxi )
            {
                maxi = cnt;
                clients_max = maxi;
            }

            if ( --fd_num <= 0 )
                continue;
        }

        for (cnt=0; cnt<=maxi; cnt++)
        {
            if ( ( fdsock = clients[ cnt ] ) < 0 )
                continue;

            if ( FD_ISSET( fdsock, &allfds ) )
            {
                memset( sock_readbuff, 0x00, DEF_SZ_READ );

                int szread = read( fdsock, sock_readbuff, DEF_SZ_READ );

                if ( szread <= 0)
                {
                    close(fdsock);
                    FD_CLR(fdsock, &readfds);
                    clients[cnt] = -1;
                }
                else
                {
                    if ( strncmp( sock_readbuff, ">>>quit", 4) == 0 )
                    {
                        //write( fdsock, "ack-close\n", 8 );
                        send( fdsock, "ack-close\n", 8, MSG_NOSIGNAL );
                        close( fdsock );
                        FD_CLR( fdsock, &readfds );
                        clients[cnt] = -1;
                        break;
                    }

                    if ( fdtty >= 0 )
                    {
                        // write socket to tty.
#ifdef DEBUG
                        printf("## TCP to TTY , %d bytes .. ", szread );
                        write( fdtty, sock_readbuff, szread );
                        printf("Ok.\n");
#else /// of DEBUG
                        write( fdtty, sock_readbuff, szread );
#endif /// of DEBUG
                    }

                }

                if ( --fd_num <= 0 )
                    break;

            }
        }
    }

    return NULL;
}

bool sigint_mutexed = false;

void sigint_handler( int signo)
{
    if ( sigint_mutexed == false )
    {
        sigint_mutexed = true;

#ifdef DEBUG
        printf("## Signal interrupted.\n");
        //signal( SIGINT, NULL );

        printf("## Terminating threads ... ");
#endif /// of DEBUG

        flag_quit = true;
        usleep( 10000 );

#ifdef DEBUG
        printf("(1)");
#endif /// of DEBUG

        if ( th_socket != NULL )
        {
            pthread_kill( th_socket, SIGINT );
            //pthread_join( th_socket, NULL );
        }

#ifdef DEBUG
        printf("(2)");
#endif /// of DEBUG
        if ( th_serial != NULL )
        {
            pthread_kill( th_serial, SIGINT );
            //pthread_join( th_serial, NULL );
        }

        close_port();
        close_socket();
#ifdef DEBUG
        printf("(done)\n");
        printf("\n#Signal interruption done.##\n");
#endif /// of DEBUG
        exit( 0 );

    }
}

void parseparams( int argc, char** argv )
{
    if ( argc > 1 )
    {
        for( int cnt=0; cnt<argc; cnt++ )
        {
            if ( strcmp( argv[cnt], "--version" ) == 0 )
            {
                opt_version = true;
            }
            else
            if ( strcmp( argv[cnt], "--daemon" ) == 0 )
            {
                opt_run_d = true;
                opt_noprint = true;
            }
            else
            if ( strcmp( argv[cnt], "--mute" ) == 0 )
            {
                opt_noprint = true;
            }
            else
            if ( strcmp( argv[cnt], "--help" ) == 0 )
            {
                opt_help = true;
            }
            else
            if ( strcmp( argv[cnt], "--forcercv" ) == 0 )
            {
                opt_forcer = true;
            }
        }
    }
}

int main( int argc, char** argv )
{
    printf( "%s: Pass-thru IrDA2TCP daemon for AnyStream. version %s\n",
            DEF_APP_NAME,
            DEF_APP_VERSION );
    printf( "(C)Copyright 2015, 3IWare - rage.kim@3iware.com\n" );
    printf( "\n" );

    parseparams( argc, argv );

    if ( opt_help == true )
    {
        printf("\n");
        printf("\tusage:\n");
        printf("\t%s [options]\n", argv[0]);
        printf("\t\t--versions : shows version of this, then program quits.\n");
        printf("\t\t--help     : shows this help, then program quits.\n");
        printf("\t\t--mute     : verbose off.\n");
        printf("\t\t--forcercv : send receive mode on to MCU when program starts.\n");
        exit( 0 );
    }

    if ( opt_version == true )
    {
        printf(" version = %s\n", DEF_APP_VERSION );
        exit( 0 );
    }

    signal( SIGINT, sigint_handler );

#ifdef DEBUG
    printf("# Open: serial ... ");
#endif
    if ( open_port() == false )
    {
        printf( ":Error: serial not be open.\n" );
        return 0;
    }
#ifdef DEBUG
    printf("Ok.\n");
#endif

    if ( opt_forcer == true )
    {
        printf( "Sending receive mode on...");

        unsigned char pkt[7] = {0};

        mk_record_on_off_command( pkt, 0x01 );
        int wln = write( fdtty, pkt, 7 );

        printf( "Ok (%d).\n", wln );
    }

#ifdef DEBUG
    printf("# Open: TCP port ... ");
#endif
    if ( open_socket() == false )
    {
        printf( ":Error: TCP port not be open.\n" );
        return 0;
    }
#ifdef DEBUG
    printf("Ok.\n");
#endif // DEBUG

    int reti = -1;

#ifdef DEBUG
    printf("-- Creating serial thread ... ");
#endif
    reti = pthread_create( &th_serial, NULL, do_loop_serial, NULL );
#ifdef DEBUG
    if ( reti >= 0 )
    {
        printf("Ok.\n");
    }
    else
    {
        printf("Failure : %d\n", reti );
        return reti;
    }
#endif // DEBUG

#ifdef DEBUG
    printf("-- Creating socket server thread ... ");
#endif // DEBUG
    reti = pthread_create( &th_socket, NULL, do_loop_socket, NULL );
#ifdef DEBUG
    if ( reti >= 0 )
    {
        printf("Ok.\n");
    }
    else
    {
        printf("Failure : %d\n", reti );
        return reti;
    }
#endif

    pthread_join( th_socket, NULL );
    pthread_join( th_serial, NULL );

#ifdef DEBUG
    printf("-- Finishing work ... ");
#endif // DEBUG

    close_port();
    close_socket();

    printf("\n");
    printf("[%s] Terminated.\n", DEF_APP_NAME);
    printf("\n");

    return 0;
}
