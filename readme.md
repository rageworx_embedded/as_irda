### Anystreaming IRDA control daemon ###
 * it named as 'as_irda' on local build path.
 * You may run a script to './init.sh' to initializing local directories to complete 'make'.

#### requirements ####
 * Your cross compiler, it must be NXP Mozart tool-chain , version 4.3.5
 * Test compiler runs as well by this command:
`${TOOLSDIR}/arm-linux-g++ --version`
 * Then you may see following texts:
```
   arm-linux-g++ (Buildroot 2010.11) 4.3.5
   Copyright (C) 2008 Free Software Foundation, Inc.
   This is free software; see the source for copying conditions.
   There is NO warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
```
 * If not, you need prepare your cross-compiler a.k.a 'Mozart ToolChain'.

#### Compilation ####
 * Just type a 'make' for compile all.
 * Remember, you had initialize all required directories by init.sh at once.

#### Install ####
 * Just type a 'make install' or , './cp2nfs.sh --release'.
 * You may need to prepare your NFS folder at '${HOME}/nfs_share'.